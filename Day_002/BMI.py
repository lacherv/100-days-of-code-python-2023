# # BMI Calculator
# height = input("enter your height in m: ")
# weight = input("enter your weight in kg: ")


# weight_as_int = int(weight)
# height_as_float = float(height)

# # using the exponent operator **
# # bmi = weight_as_int / height_as_float ** 2

# # or using multiplication and PEDMAS
# bmi = weight_as_int / (height_as_float * height_as_float)

# bmi_as_int = int(bmi)

# print(bmi_as_int)

# # bmi = int(weight) / float(height) ** 2
# # bmi_int = int(bmi)

weight = int(input())
height = float(input())
bmi = weight/float(height*height)
if bmi < 18.5:
    print("Your BMI is " + str(bmi) + " Underweight")
elif bmi >= 18.5 and bmi < 25:
    print("Your BMI is " + str(bmi) + " Normal")
elif bmi >= 25 and bmi < 30:
    print("Your BMI is " + str(bmi) + " Overweight")
else:
    print("Your BMI is " + str(bmi) + " Obesity")
