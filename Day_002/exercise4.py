#  Round the number
print(round(8/3, 2))
print(round(2.666666666666666, 4))

# Interger Division
print(8 // 3)
print(type(8 // 3))

# Increment
score = 0

score += 1  # user scores a point.
print(score)

#  F-string
height = 1.8
isWinning = True

print(
    f"Your score is {score}, your height is {height}, you are winning is {isWinning} ")
