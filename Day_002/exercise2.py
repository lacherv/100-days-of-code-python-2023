# 🚨 Don't change the code below 👇
two_digit_number = input("Type a two digit number: ")

####################################

# Check the data type of two_digit_number
print(type(two_digit_number))

# Get the first and second digits using subscripting then convert string to int.
first_digit = two_digit_number[0]
second_digit = two_digit_number[1]

# Add the two digits together
result = int(first_digit) + int(second_digit)
print(result)
