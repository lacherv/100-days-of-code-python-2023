# 100 Days of Code Python 2023

## The complete Python Bootcamp for 2023 with Dr Angela Yu

### Day 001

- Python Print Function
- String Manipulation
- Input and Print function

### Day 002

- Primitive Data Types
- Type Error, Type Checking and Type Conversion
- Mathematical Operations **PEMDAS**
- Numnber Manipulation and F-string
